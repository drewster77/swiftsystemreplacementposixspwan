//
//  main.swift
//  SwiftSystemReplacementPosixSpwan
//
//  Created by Andrew Benson on 3/10/19.
//  Copyright © 2019 Andrew Benson. All rights reserved.
//

import Foundation

func system(_ command: String) {
    let argsArray = command.split(separator: " ").map { (substr) -> String in
        return String(substr)
    }
    guard argsArray.count != 0 else { return  }

    let command = strdup(argsArray.first!)
    let args = argsArray.map { strdup($0) } + [nil]
    posix_spawn(nil, command, nil, nil, args, nil)
    return
}

func shellInterpretedSystem(_ command: String) {
    let argsArray = [
        "/bin/sh",
        "-c",
        command
        ].map { (substr) -> String in
            return String(substr)
    }
    guard argsArray.count != 0 else { return  }

    let command = strdup(argsArray.first!)
    let args = argsArray.map { strdup($0) } + [nil]
    posix_spawn(nil, command, nil, nil, args, nil)
    return
}

system("/bin/ls -l /Users/*")
system("/bin/echo you are cool man")
shellInterpretedSystem("ls -l /Users/*")

